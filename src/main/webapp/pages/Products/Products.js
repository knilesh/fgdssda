Application.$controller("ProductsPageController", ["$rootScope", "$scope", "Widgets", "Variables", "$location", "DialogService", "$routeParams",
    function($rootScope, $scope, Widgets, Variables, $location, DialogService, $routeParams) {
        "use strict";

        function setActiveProduct(allProducts) {
            if (allProducts && Variables.selectedItem) {
                Variables.selectedItem.dataSet = allProducts.filter(function (item) {
                    return item.id === $scope.activeProductId;
                })[0];
            }
        }

        /** This is a callback function, that is called after a page is rendered.*/
        $scope.onPageReady = function () {
            $rootScope.pageLoading = false;
        };

        /**This is a callback function, that is called after the page variables are loaded. */
        $scope.onPageVariablesReady = function () {
            setActiveProduct(Variables.allProducts.dataSet.data);
            delete Variables.currentUser.dataSet.dataValue;
        };

        /**This is a callback function, when the page is loaded with the url route parameters. */
        $scope.handleRoute = function (productId) {
            $scope.activeProductId = +productId;
        };

        $rootScope.allProductsonSuccess = function (variable, data) {
            setActiveProduct(data);
        };

        /*method to handle add to cart click and insert item to db*/
        $scope.addToCartClick = function ($event, $scope) {
            if ($rootScope.isUserAuthenticated) {
                $rootScope.pageLoading = true;
                $rootScope.addItemToCart();
            } else {
                $rootScope.loginBeforeCart = true;
                DialogService.showDialog("CommonLoginDialog");
            }
        };

    }
]);