Application.$controller("topnavPageController", ["$scope", "Widgets", "Variables", "$routeParams", function ($scope, Widgets, Variables, $routeParams) {
    "use strict";
    $scope.activePage = $routeParams.name;

    $scope.navigateToPage = function ($event, page, $scope) {
        if($scope.activePage === page){
            return;
        }
        $scope.$root.pageLoading = true;
    }
}]);
