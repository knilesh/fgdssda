Application.run(function ($rootScope, Widgets, Variables, $location, $route) {
    "use strict";

    $rootScope.selectedItem = {};
    $rootScope.isAdminLoggedIn = false;

    $rootScope.pageLoading = false;

    /* perform any action with the variables inside this block(on-page-load) */
    $rootScope.onAppVariablesReady = function () {
        /*
         * variables can be accessed through 'Variables' service here
         * e.g. Variables.staticVariable1.getData()
         */
    };

    $rootScope.activateProductSearch = function ($event) {
        $event.currentTarget.style.width = "320px";
    };

    $rootScope.productSearchSubmit = function ($event, $scope) {
        /*Return if no "item" has been selected in the Search box.*/
        if (!$event.data.item) {
            return;
        }

        $rootScope.selectedItem = $event.data.item;
        $rootScope.selectedItem.imgUrl = $rootScope.selectedItem.imgUrl.replace("Thumbnails", "Images");
        $location.path("Products/" + $rootScope.selectedItem.id);
        $rootScope.pageLoading = true;
    };

    $rootScope.productSearchonBeforeUpdate = function(variable, data){
        WM.forEach(data, function(product) {
            product.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + product.imgUrl;
        });
    };

    $rootScope.addItemToCart = function () {
        Variables.currentProduct.dataSet = Variables.selectedItem.dataSet;
        var item = {
            "product": Variables.currentProduct.dataSet,
            "quantity": 1,
            "status": "In Cart",
            "user": Variables.currentUser.dataSet
        };
        Variables.call("insertRow", "cartInsert", {
            "row": item
        }, function(response) {
        });

        $location.path("Cart");
    };

    $rootScope.logoutVariableonSuccess = function (variable, data) {
        $rootScope.isAdminLoggedIn = undefined;
        $route.current.params.name === "Main" ? $route.reload() : $location.path('Main');
    };

    $rootScope.currentUserIdonSuccess = function(variable, data){
        Variables.currentUser.dataSet = {};
        if(Variables.users.dataSet.data && data) {
            Variables.currentUser.dataSet = Variables.users.dataSet.data.filter(function(user) {
                return user.id === +data;
            })[0];
        }
    };

});