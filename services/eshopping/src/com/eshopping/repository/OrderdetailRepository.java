package com.eshopping.repository; 
// Generated 23 Jun, 2014 4:50:28 PM 

import com.wavemaker.runtime.data.dao.WMGenericDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.eshopping.*;
/**
 * Specifies methods used to obtain and modify Orderdetail related information
 * which is stored in the database.
 */
@Repository("eshopping.OrderdetailDao")
public class OrderdetailRepository extends WMGenericDaoImpl <Orderdetail, Integer> {

   @Autowired
   @Qualifier("eshoppingTemplate")
   private HibernateTemplate template;

   public HibernateTemplate getTemplate() {
        return this.template;
   }
}

