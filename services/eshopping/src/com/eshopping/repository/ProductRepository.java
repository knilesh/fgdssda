package com.eshopping.repository; 
// Generated 23 Jun, 2014 4:50:28 PM 

import com.wavemaker.runtime.data.dao.WMGenericDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.eshopping.*;
/**
 * Specifies methods used to obtain and modify Product related information
 * which is stored in the database.
 */
@Repository("eshopping.ProductDao")
public class ProductRepository extends WMGenericDaoImpl <Product, Integer> {

   @Autowired
   @Qualifier("eshoppingTemplate")
   private HibernateTemplate template;

   public HibernateTemplate getTemplate() {
        return this.template;
   }
}

