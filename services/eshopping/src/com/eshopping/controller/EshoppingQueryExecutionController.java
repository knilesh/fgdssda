package com.eshopping.controller;
// Generated 23 Jun, 2014 4:50:28 PM 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Qualifier;

import com.eshopping.service.QueryExecutorService;
import com.wavemaker.runtime.data.model.CustomQuery;
import com.wavemaker.runtime.data.exception.QueryParameterMismatchException;

@RestController
@RequestMapping("/eshopping/queryExecutor")
public class EshoppingQueryExecutionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EshoppingQueryExecutionController.class);

	@Autowired
	@Qualifier("eshopping.queryExecutorService")
	private QueryExecutorService queryService;
	
	
	@ExceptionHandler(QueryParameterMismatchException.class)
	ResponseEntity<String> handleNamedQueryParameterMismatchException(
			Exception e) {
		return new ResponseEntity<String>(String.format("{\"Error\":\"%s\"}",
				e.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(value = "/queries/wm_custom", method = RequestMethod.POST)
	public Page<Object> executeWMCustomQuery(@RequestBody CustomQuery query,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		Pageable pageable = new PageRequest(page - 1, size);
		Page<Object> result = queryService.executeWMCustomQuery(query, pageable);
		LOGGER.debug("got the result {}" + result);
		return result;
	}
}
