package com.eshopping.controller; 

// Generated 23 Jun, 2014 4:50:28 PM


import com.eshopping.service.ItemorderService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eshopping.*;

/**
 * Controller object for domain model class Itemorder.
 * @see com.eshopping.Itemorder
 */

@RestController
@RequestMapping("/eshopping/Itemorder")
public class EshoppingItemorderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EshoppingItemorderController.class);

	@Autowired
	@Qualifier("eshopping.ItemorderService")
	private ItemorderService service;

	/**
	 * Processes requests to return lists all available Itemorders.
	 * 
	 * @param model
	 * @return The name of the  Itemorder list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Itemorder> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Itemorders list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Itemorder> getItemorders(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Itemorders list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Itemorders");
		Long count = service.countAll();
		return count;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Itemorder getItemorder(@PathVariable("id") Integer id) throws EntityNotFoundException {
		LOGGER.debug("Getting person with id: {}" , id);
		Itemorder instance = service.findById(id);
		LOGGER.debug("Itemorder details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
		LOGGER.debug("Deleting Itemorder with id: {}" , id);
		Itemorder deleted = service.delete(id);
		return deleted != null;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Itemorder editItemorder(@PathVariable("id") Integer id, @RequestBody Itemorder instance) throws EntityNotFoundException {
        LOGGER.debug("Editing Itemorder with id: {}" , instance.getId());
        instance.setId(id);
		instance = service.update(instance);
		LOGGER.debug("Itemorder details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Itemorder createItemorder(@RequestBody Itemorder instance) {
		LOGGER.debug("Create Itemorder with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Itemorder with information: {}" , instance);
	    return instance;
	}
	
	@org.springframework.web.bind.annotation.ExceptionHandler(EntityNotFoundException.class)
	ResponseEntity<String> handleEntityNotFoundException(Exception e) {
		return new ResponseEntity<String>(String.format("{\"Error\":\"%s\"}",
				e.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setItemorderService(ItemorderService service) {
		this.service = service;
	}
}

